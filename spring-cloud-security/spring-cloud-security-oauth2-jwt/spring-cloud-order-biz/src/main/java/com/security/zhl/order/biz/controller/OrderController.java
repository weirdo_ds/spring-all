package com.security.zhl.order.biz.controller;

import com.security.zhl.order.biz.model.UserDTO;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class OrderController {

    @GetMapping(value = "/r1")
    @PreAuthorize("hasAnyAuthority('p1')")
    public  String r1(){

        UserDTO userDTO = (UserDTO) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        System.out.println(userDTO.getUsername());
        return "访问资源r1";
    }

    @GetMapping(value = "/r2")
    @PreAuthorize("hasAnyAuthority('p3')")
    public  String r2(){
        return "访问资源r2";
    }
}
