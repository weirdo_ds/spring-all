package com.security.zhl.order.biz;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableDiscoveryClient
@EnableHystrix
@EnableFeignClients(basePackages = {"com.security.zhl.order.biz"})
public class SecurityOrderApplication {
    public static void main(String[] args) {
        SpringApplication.run(SecurityOrderApplication.class , args );
        System.out.println("finsh.");
    }
}
