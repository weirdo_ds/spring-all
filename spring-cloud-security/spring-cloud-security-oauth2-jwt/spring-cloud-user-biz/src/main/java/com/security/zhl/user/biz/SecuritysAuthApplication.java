package com.security.zhl.user.biz;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;
import org.springframework.cloud.openfeign.EnableFeignClients;

@MapperScan("com.security.zhl.user.biz.mapper")
@SpringBootApplication
@EnableDiscoveryClient
@EnableHystrix
@EnableFeignClients(basePackages = {"com.security.zhl.user.biz"})
public class SecuritysAuthApplication {
    public static void main(String[] args) {
        SpringApplication.run(SecuritysAuthApplication.class , args );
        System.out.println("finsh.");
    }

}

