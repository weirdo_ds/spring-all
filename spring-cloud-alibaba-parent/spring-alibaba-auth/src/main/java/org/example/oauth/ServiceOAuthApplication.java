package org.example.oauth;

import com.alibaba.csp.sentinel.annotation.SentinelResource;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@EnableDiscoveryClient
@MapperScan("org.example.oauth.mapper")
@EnableHystrix
@EnableFeignClients(basePackages = {"org.example.oauth"})
public class ServiceOAuthApplication {

    public static void main(String[] args) {
        SpringApplication.run(ServiceOAuthApplication.class, args);
        System.out.println("finsh.");
    }

    @Value("${server.port}")
    private String port;

    @RestController
    public class EchoController {
        @GetMapping(value = "/echo/{name}")
        @SentinelResource("hello")
        public String echo(@PathVariable String name) {
            return "Hello ServiceUserApplication Discovery " + name +"\t"+port;
        }
    }


}
