package org.example.oauth.mapper;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.example.oauth.entity.TPermission;
import org.example.oauth.entity.TUser;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface TUserMapper {
    @Select("select * from t_user where username=#{username}")
    TUser findByUserName(@Param("username") String username) ;

    @Select("select * from t_permission where id in (" +
            "select rp.permission_id from t_role_permission rp where rp.role_id in (" +
            "select r.role_id from t_user_role r where r.user_id = #{userId}))")
    List<TPermission> findPermissionByUserId(@Param("userId") String userId) ;
}


