package com.alibaba.zhl.consumer.feign;

import org.springframework.web.bind.annotation.PathVariable;

public class EchoServiceFallback implements EchoService{
    public String echo(@PathVariable("name") String name) {
        return "echo fallback";
    }
}
