package com.alibaba.zhl.consumer.config;

import com.alibaba.zhl.consumer.feign.EchoServiceFallback;
import org.springframework.context.annotation.Bean;

public class FeignConfiguration {
    @Bean
    public EchoServiceFallback echoServiceFallback() {
        return new EchoServiceFallback();
    }
}
