package com.alibaba.zhl.consumer.controller;

import com.alibaba.zhl.consumer.feign.EchoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class EchoController {

    @Autowired
    private EchoService echoService;

    @GetMapping(value = "/echo/{name}")
    public String echo(@PathVariable String name) {

        System.out.println("#############"+name);
        return echoService.echo(name);
    }
}
