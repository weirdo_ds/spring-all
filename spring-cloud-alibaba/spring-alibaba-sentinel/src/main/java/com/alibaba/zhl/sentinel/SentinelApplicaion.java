package com.alibaba.zhl.sentinel;

import com.alibaba.csp.sentinel.annotation.SentinelResource;
import org.springframework.boot.SpringApplication;
import org.springframework.cloud.client.SpringCloudApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringCloudApplication
public class SentinelApplicaion {
    public static void main(String[] args) {
        SpringApplication.run(SentinelApplicaion.class, args);
        System.out.println("finsh.");
    }

    @RestController
    public class TestController {
        @GetMapping(value = "/hello")
        @SentinelResource("hello")
        public String hello() {
            return "Hello Sentinel";
        }
    }
}
