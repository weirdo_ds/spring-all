package org.com.zhl.consumer;

import org.com.zhl.consumer.feign.ProduceHelloService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
@EnableDiscoveryClient
@EnableFeignClients
@SpringBootApplication
public class ConsumerApplication {
    public static void main(String[] args) {
        SpringApplication.run(ConsumerApplication.class , args  );
        System.out.println("finsh.");
    }

    @Autowired
    private ProduceHelloService produceHelloService;


    @GetMapping(value = "/echo/{name}")
    public String echo(@PathVariable String name) {
        System.out.println("#############"+name);
        return produceHelloService.echo(name);
    }
}
