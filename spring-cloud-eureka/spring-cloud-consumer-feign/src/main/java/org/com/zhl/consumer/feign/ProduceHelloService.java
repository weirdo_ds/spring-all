package org.com.zhl.consumer.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name= "server-provider")
public interface ProduceHelloService {

    @GetMapping(value = "/echo/{name}")
    String echo(@PathVariable("name") String name);

}
