package com.jdbc.controller;

import com.baomidou.mybatisplus.extension.api.R;
import com.jdbc.entity.TUser;
import com.jdbc.service.UserDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

@RestController
@RequestMapping("/admin/api")
public class AdminController {

    @Autowired
    private UserDetailService userDetailService;

    @GetMapping("hello")
    public String hello(){
        return "hello admin";
    }

    @GetMapping("demo")
    @ResponseBody
    public TUser demo(){
        TUser tUser = userDetailService.getUser("admin");
        System.out.println(tUser);
        return tUser;
    }

    @GetMapping("redis")
    @ResponseBody
    public R redis(){
        TUser tUser = userDetailService.getUser1("user");
        System.out.println(tUser);
        R r = R.ok(tUser);
        return r;
    }
}
