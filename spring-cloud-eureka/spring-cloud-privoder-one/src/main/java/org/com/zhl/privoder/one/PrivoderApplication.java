package org.com.zhl.privoder.one;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
@EnableEurekaClient
public class PrivoderApplication {

    @Value("${server.port}")
    private String port;

    public static void main(String[] args) {
        SpringApplication.run(PrivoderApplication.class , args );
        System.out.println("finsh.");
    }

    @RestController
    public class EchoController {
        @GetMapping(value = "/echo/{name}")
        public String echo(@PathVariable String name) {
            return "Hello Nacos Discovery " + name +"\t"+port;
        }
    }

}
