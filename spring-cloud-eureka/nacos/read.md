kubernetes安装部署nacos集群步骤记录
1、安装nfs,具体步骤
https://www.cnblogs.com/xulikai88/p/9428959.html

client查看共享目录

    showmount -e 192.168.200.30

    mount -t nfs 192.168.200.30:/usr/local/work/share  /usr/local/work/share  -o proto=tcp -o nolock

---保证重启后配置不失效
更改client服务器挂载文件/etc/fstab

    192.168.200.30:/usr/local/work/share /usr/local/work/share nfs defaults 0 0

2、将nfs安装至k8s中，具体操作配置在nfs文件目录中
分别执行

    kubectl create -f rbac.yaml
    kubectl create -f deployment.yaml
    kubectl create -f class.yaml

3、接下来安装master / slave数据库(nfs)挂接
执行文件 mysql目录中

    kubectl create -f mysql-slave-nfs.yaml
    kubectl create -f mysql-master-nfs.yaml

4、安装nacos

    执行
    kubectl create -f nacos-pvc-nfs.yaml
---如果启动时显示数据库连接有问题，则需要更新数据库驱动，安装的数据是mysql8,则驱动以要更新为8的版本

5、自己将源代码取下来，更新数据库的驱动版本，在本地测试连接，正常后进行打包
    
     docker build -t nacos-console:1.0.0 .
     
     删除镜像：
     docker rmi 192.168.200.32/pub/nacos-console:1.0.0
6、打包成功后进行镜像推动

    docker tag nacos-console:1.0.0 192.168.200.32/pub/nacos-console:1.0.0
    然后进行docker 登录
    docker login 192.168.200.32
    输入用户名密码，成功后进行推动镜像
    docker push 192.168.200.32/pub/nacos-console:1.0.1
    
    




