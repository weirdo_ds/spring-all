import org.junit.Test;

import java.util.HashMap;

public class HashMapTest {

    /*transient volatile int modCount;
    final float loadFactor;
    int threshold;
    static final float DEFAULT_LOAD_FACTOR = 0.75f;
    static final int MAXIMUM_CAPACITY = 1 << 30;
    static final int DEFAULT_INITIAL_CAPACITY = 16;*/

    @Test
    public void test1(){
        System.out.println("finsh.");
        int h = hash(12);
        HashMap map = new HashMap();
        System.out.println(h);


    }

    /**
     * 1.6版本的Hash方法
     * @param h
     * @return
     */
    static int hash(int h) {
        // This function ensures that hashCodes that differ only by
        // constant multiples at each bit position have a bounded
        // number of collisions (approximately 8 at default load factor).
        h ^= (h >>> 20) ^ (h >>> 12);
        return h ^ (h >>> 7) ^ (h >>> 4);
    }

    /**
     * 1.8
     * @param key
     * @return
     */
    static final int hash(Object key) {
        int h;
        return (key == null) ? 0 : (h = key.hashCode()) ^ (h >>> 16);
    }

    /**获得数据下标
     * Returns index for hash code h.
     */
    static int indexFor(int h, int length) {
        return h & (length-1);
    }
    /**
     *jdk 1.6版本学习
     * @param key
     * @return
     * 1、先判断 key是否为空，如果为空，则获取 null获取的值
     * getForNullKey（） -> 首先获取下标地址为0 的对象，再判断Entry是否为空，如果不为空，则继续查询链表下一个节点，直接 key == null,条件成立 ，说明已经找到，将value中数据返回 ，否则返回null
     * 2、条件不为空，则将key取hashCode(),即Object的HashCode()方法，再将取hash,得到一个hash值
     * 3、开始循环遍历数据->> 先根据 indexFor(hash, table.length) 来取得key所在的下标位置 具体实现 h & (length-1); （hash值数据长度进行按位& 操作） , 并得到一个链接，然后遍历链接
     * 通过当前entity的hash值与 之前key所算出来的hash进行比较e.hash == hash,并且当前链表的key与传递进来的key进行比较(k = e.key) == key || key.equals(k) ，
     * 4、条件成立，则直接返回当前entity的value值
     * 5、直接最后返回null;
     *
     */
    /*public V get(Object key) {
        if (key == null)
            return getForNullKey();
        int hash = hash(key.hashCode());
        for (HashMap.Entry<K,V> e = table[indexFor(hash, table.length)];
             e != null;
             e = e.next) {
            Object k;
            if (e.hash == hash && ((k = e.key) == key || key.equals(k)))
                return e.value;
        }
        return null;
    }*/

    /**
     * 1.8
     */
    /*public V get(Object key) {
        HashMap.Node<K,V> e;
        return (e = getNode(hash(key), key)) == null ? null : e.value;
    }
    final Node<K,V> getNode(int hash, Object key) {
        Node<K,V>[] tab; Node<K,V> first, e; int n; K k;
        if ((tab = table) != null && (n = tab.length) > 0 &&
            (first = tab[(n - 1) & hash]) != null) {
            if (first.hash == hash && // always check first node
                ((k = first.key) == key || (key != null && key.equals(k))))
                return first;
            if ((e = first.next) != null) {
                if (first instanceof TreeNode)
                    return ((TreeNode<K,V>)first).getTreeNode(hash, key);
                do {
                    if (e.hash == hash &&
                        ((k = e.key) == key || (key != null && key.equals(k))))
                        return e;
                } while ((e = e.next) != null);
            }
        }
        return null;
    }
    */


    /**
     * jdk1.6版本
     * @param key
     * @param value
     * @return
     * 1、判断key是否为空，如果为空，则将value存放到key为空的位置，即下标为0号位置，具体方法putForNullKey(value)
     */

   /* public V put(K key, V value) {
        //判断Key是否为空，如果为空，则存储位置为table[0]或者 table[0]的冲突链上
        if (key == null)
            return putForNullKey(value);
        //对key的hashCode进一步计算，确保散列均匀
        int hash = hash(key.hashCode());
        //根据 hash值及table长度来获取 table中的实际位置
        int i = indexFor(hash, table.length);
        for (HashMap.Entry<K,V> e = table[i]; e != null; e = e.next) {
            //如果该对应位置已经存在该值，则进行覆盖操作。用新value替换旧value，并返回旧value
            Object k;
            if (e.hash == hash && ((k = e.key) == key || key.equals(k))) {
                V oldValue = e.value;
                e.value = value;
                e.recordAccess(this);
                return oldValue;
            }
        }

        modCount++; //保证并发访问时，若hashmap内部结构发生变化，快速响应失败
        addEntry(hash, key, value, i); //新增一个entity
        return null;
    }*/

    /**
     * @param value
     * @return
     * 1、获取0号位置的链接，开始遍历链接，如果链接不为空，判断e.key == null ,
     * 即尾插法
     */

   /* private V putForNullKey(V value) {
        for (HashMap.Entry<K,V> e = table[0]; e != null; e = e.next) {
            if (e.key == null) {
                V oldValue = e.value;
                e.value = value;
                e.recordAccess(this);
                return oldValue;
            }
        }
        modCount++;
        addEntry(0, null, value, 0);
        return null;
    }*/

    /*
    public V put(K key, V value) {
        return putVal(hash(key), key, value, false, true);
    }
    final V putVal(int hash, K key, V value, boolean onlyIfAbsent,
                   boolean evict) {
        HashMap.Node<K,V>[] tab; HashMap.Node<K,V> p; int n, i;
        if ((tab = table) == null || (n = tab.length) == 0)
            n = (tab = resize()).length;
        if ((p = tab[i = (n - 1) & hash]) == null)
            tab[i] = newNode(hash, key, value, null);
        else {
            HashMap.Node<K,V> e; K k;
            if (p.hash == hash &&
                    ((k = p.key) == key || (key != null && key.equals(k))))
                e = p;
            else if (p instanceof HashMap.TreeNode)
                e = ((HashMap.TreeNode<K,V>)p).putTreeVal(this, tab, hash, key, value);
            else {
                for (int binCount = 0; ; ++binCount) {
                    if ((e = p.next) == null) {
                        p.next = newNode(hash, key, value, null);
                        if (binCount >= TREEIFY_THRESHOLD - 1) // -1 for 1st
                            treeifyBin(tab, hash);
                        break;
                    }
                    if (e.hash == hash &&
                            ((k = e.key) == key || (key != null && key.equals(k))))
                        break;
                    p = e;
                }
            }
            if (e != null) { // existing mapping for key
                V oldValue = e.value;
                if (!onlyIfAbsent || oldValue == null)
                    e.value = value;
                afterNodeAccess(e);
                return oldValue;
            }
        }
        ++modCount;
        if (++size > threshold)
            resize();
        afterNodeInsertion(evict);
        return null;
    }*/

}
